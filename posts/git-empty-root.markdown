---
title: Git - add empty root
published: 2016-11-28
tags: git
description: Suspendisse pharetra ullamcorper sem et auctor. Suspendisse vitae tellus eu.
---

Rewrite Git history, so the repo will include an empty root commit

<!-- more -->

<pre><code>
# need a new empty branch; say `newroot`
git checkout --orphan newroot
git rm -rf .

# apply the same steps
git commit --allow-empty -m 'root commit'
git rebase --onto newroot --root master
git branch -d newroot 

</code></pre>